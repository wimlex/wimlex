#!/bin/bash

cd ~
source ~/config/setup.sh
RUNNING=false

if screen -list | grep -i "\.$SPIGOT_SCREENNAME"; then
	RUNNING=true
fi

if [ "$RUNNING" = "true" ]; then
	~/server/send-say.sh "Stopping the server in 1 minute"
	sleep 50s
	~/server/send-say.sh "Stopping the server in 10 seconds"
	sleep 5s
	~/server/send-say.sh "Stopping the server in 5 seconds"
	sleep 2s
	~/server/send-say.sh "Stopping the server in 3 seconds"
	sleep 1s
	~/server/send-say.sh "Stopping the server in 2 seconds"
	sleep 1s
	~/server/send-say.sh "Stopping the server in 1 second"
	sleep 1s
	~/server/send-say.sh "Saving the world"
	~/server/send-cmd.sh "save-all"
	sleep 10s
	~/server/send-say.sh "Stopping the server"
	~/server/send-cmd.sh "stop"
	sleep 10s
	screen -X -S $SPIGOT_SCREENNAME quit
	sleep 3s
fi

if [ "$SPIGOT_BACKUP" = "true" ]; then
	~/cron/git-backup.sh
fi
