#!/bin/bash

cd ~
git clone --recurse-submodules -j8 git@bitbucket.org:WouterG/wimlex.git .
source ~/config/setup.sh

if [ "$SPIGOT_UPDATE" = "true" ]; then
	~/cron/spigot-update.sh download
	~/cron/spigot-update.sh install
fi

~/server/screen-start.sh
