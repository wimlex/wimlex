#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "No file supplied"
	echo "Use $0 <command-file>"
	exit 1
fi

source ~/config/setup.sh

input="$@"
while IFS= read -r line
do
	screen -S $SPIGOT_SCREENNAME -p 0 -X stuff "$line^M"
	sleep 0.05s
done < "$input"
