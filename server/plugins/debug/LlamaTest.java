import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Llama;
import org.bukkit.entity.Player;

public class LlamaTest implements Debugger {
	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			cs.sendMessage("Execute this as a player while riding a llama");
			return;
		}
		Player p = (Player) cs;
		Entity vehicle = p.getVehicle();
		if (!(vehicle instanceof Llama)) {
			cs.sendMessage("You need to be riding a llama");
			return;
		}
		Llama llama = (Llama) vehicle;
		llama.setStrength(5);
		llama.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(28d);
	}
}
