import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class GetLoadedChunks implements Debugger, Debugger.Stoppable, Runnable {

	private RuntimeDebugger plugin;
	private BukkitTask task;

	private int count = 0;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		this.plugin = plugin;
		this.task = Bukkit.getScheduler().runTaskTimer(plugin, this, 0L, 20L * 20L);
	}

	public void sendMessage(String msg) {
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			if (count % 15 == 0) {
				//onlinePlayer.sendMessage(msg);
			} else if (onlinePlayer.getName().equalsIgnoreCase("Wouto1997")) {
				onlinePlayer.sendMessage(msg);
			}
		}
	}

	@Override
	public void run() {
		World w = Bukkit.getServer().getWorlds().get(0);
		System.out.println("Loaded chunks in world \"" + w.getName() + "\": " + w.getLoadedChunks().length);
		TreeSet<ChunkEntityCountEntry> entityEntries = new TreeSet<>((o1, o2) -> Integer.compare(o2.getEntityCount(), o1.getEntityCount()));
		TreeSet<ChunkEntityCountEntry> tileEntityEntries = new TreeSet<>((o1, o2) -> Integer.compare(o2.getEntityCount(), o1.getEntityCount()));
		for (Chunk loadedChunk : w.getLoadedChunks()) {
			entityEntries.add(new ChunkEntityCountEntry(loadedChunk, loadedChunk.getEntities().length));
			tileEntityEntries.add(new ChunkEntityCountEntry(loadedChunk, loadedChunk.getTileEntities().length));
		}

		Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
			List<PlayerChunkGroup> playerChunkGroups = new ArrayList<>();

			for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				Chunk playerChunk = onlinePlayer.getLocation().getChunk();
				PlayerChunkGroup found = null;
				for (PlayerChunkGroup playerChunkGroup : playerChunkGroups) {
					if (getChunkDistance(playerChunkGroup.getChunk(), playerChunk) < 8) {
						found = playerChunkGroup;
					}
				}
				if (found == null) {
					found = new PlayerChunkGroup(new ArrayList<>(Arrays.asList(onlinePlayer)), playerChunk);
					playerChunkGroups.add(found);
				} else {
					found.getPlayers().add(onlinePlayer);
				}
			}

			List<PlayerChunkGroupWithEntries> playerChunkGroupWithEntries = new ArrayList<>();

			for (PlayerChunkGroup playerChunkGroup : playerChunkGroups) {
				PlayerChunkGroupWithEntries entries = new PlayerChunkGroupWithEntries(playerChunkGroup);
				playerChunkGroupWithEntries.add(entries);
				for (ChunkEntityCountEntry entityEntry : entityEntries) {
					if (getChunkDistance(playerChunkGroup.getChunk(), entityEntry.getChunk()) < 8) {
						entries.getEntityEntries().add(entityEntry);
					}
				}
				for (ChunkEntityCountEntry tileEntityEntry : tileEntityEntries) {
					if (getChunkDistance(playerChunkGroup.getChunk(), tileEntityEntry.getChunk()) < 8) {
						entries.getTileEntityEntries().add(tileEntityEntry);
					}
				}
			}

			Bukkit.getScheduler().runTask(this.plugin, () -> {
				count++;
				sendMessage(
						ChatColor.DARK_RED + "[" + ChatColor.RED + "EntityCounter" + ChatColor.DARK_RED + "] " +
								ChatColor.GOLD + "Found " +
								ChatColor.AQUA + Bukkit.getOnlinePlayers().size() +
								ChatColor.GOLD + " online players, grouped in " +
								ChatColor.AQUA + playerChunkGroupWithEntries.size() +
								ChatColor.GOLD + " player groups."
				);
				sendMessage(
						ChatColor.GOLD + "Here are the player groups with entity counts:"
				);
				for (PlayerChunkGroupWithEntries playerChunkGroupWithEntry : playerChunkGroupWithEntries) {
					printPlayerChunkGroupWithEntry(playerChunkGroupWithEntry);
				}
			});
		});
	}

	private void printPlayerChunkGroupWithEntry(PlayerChunkGroupWithEntries playerChunkGroupWithEntries) {
		List<Player> players = playerChunkGroupWithEntries.getPlayerChunkGroup().getPlayers();
		List<String> playerNamesList = players.stream().map(Player::getName).collect(Collectors.toList());
		String playerNames = String.join(", ", playerNamesList);
		sendMessage(
				ChatColor.GOLD + "Players: " +
						ChatColor.AQUA + playerNames +
						ChatColor.GOLD + " are surrounded by " +
						ChatColor.AQUA + playerChunkGroupWithEntries.getTotalEntities() +
						ChatColor.GOLD + " entities and " +
						ChatColor.AQUA + playerChunkGroupWithEntries.getTotalTileEntities() +
						ChatColor.GOLD + " tile entities, in " +
						ChatColor.AQUA + Math.max(
								playerChunkGroupWithEntries.getEntityEntries().size(),
								playerChunkGroupWithEntries.getTileEntityEntries().size()
						) + ChatColor.GOLD + " chunks"
		);
	}

	public double getChunkDistance(Chunk playerChunk, Chunk otherChunk) {
		return Point2D.distance(playerChunk.getX(), playerChunk.getZ(), otherChunk.getX(), otherChunk.getZ());
	}

	private void printEntry(ChunkEntityCountEntry entry, String name) {
		System.out.println(String.format(
				"chunk: {x: %d, y: %d, %s: %d}",
				entry.getChunk().getX() << 4,
				entry.getChunk().getZ() << 4,
				name,
				entry.getEntityCount()
		));
	}

	@Override
	public void stop() {
		if (this.task != null) {
			this.task.cancel();
		}
	}

	static class PlayerChunkGroupWithEntries {

		private PlayerChunkGroup playerChunkGroup;
		private List<ChunkEntityCountEntry> entityEntries;
		private List<ChunkEntityCountEntry> tileEntityEntries;

		public PlayerChunkGroupWithEntries(PlayerChunkGroup playerChunkGroup) {
			this.playerChunkGroup = playerChunkGroup;
			this.entityEntries = new ArrayList<>();
			this.tileEntityEntries = new ArrayList<>();
		}

		public PlayerChunkGroup getPlayerChunkGroup() {
			return playerChunkGroup;
		}

		public List<ChunkEntityCountEntry> getEntityEntries() {
			return entityEntries;
		}

		public List<ChunkEntityCountEntry> getTileEntityEntries() {
			return tileEntityEntries;
		}

		public int getTotalEntities() {
			int total = 0;
			for (ChunkEntityCountEntry entityEntry : this.getEntityEntries()) {
				total += entityEntry.getEntityCount();
			}
			return total;
		}

		public int getTotalTileEntities() {
			int total = 0;
			for (ChunkEntityCountEntry tileEntityEntry : this.getTileEntityEntries()) {
				total += tileEntityEntry.getEntityCount();
			}
			return total;
		}

	}

	static class PlayerChunkGroup {
		private List<Player> players;
		private Chunk chunk;

		public PlayerChunkGroup(List<Player> players, Chunk chunk) {
			this.players = players;
			this.chunk = chunk;
		}

		public List<Player> getPlayers() {
			return players;
		}

		public void setPlayers(List<Player> players) {
			this.players = players;
		}

		public Chunk getChunk() {
			return chunk;
		}

		public void setChunk(Chunk chunk) {
			this.chunk = chunk;
		}
	}

	static class ChunkEntityCountEntry {
		private Chunk chunk;
		private int entityCount;

		public ChunkEntityCountEntry(Chunk chunk, int entityCount) {
			this.chunk = chunk;
			this.entityCount = entityCount;
		}

		public Chunk getChunk() {
			return chunk;
		}

		public int getEntityCount() {
			return entityCount;
		}
	}

}
