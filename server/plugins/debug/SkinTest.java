import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.Packet;
import net.minecraft.server.v1_14_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_14_R1.PacketPlayOutRespawn;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Set;

public class SkinTest implements Debugger {

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			cs.sendMessage("Run as player");
			return;
		}
		Player p = (Player) cs;
		p.setCustomName("Wouto1997");
		p.setPlayerListName("Wouto1997");
	}

	private static Location getYeet(Player player) {
		Location clone = player.getLocation().clone();
		clone.setY(10000);
		return clone;
	}

	public static final void reloadSkinForSelf(JavaPlugin plugin, Player player) {
		Set<Player> nearby = getNearbyPlayersToUpdate(player);
		Location yeet = getYeet(player);
		final EntityPlayer ep = ((CraftPlayer) player).getHandle();
		final PacketPlayOutPlayerInfo removeInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ep);
		final PacketPlayOutPlayerInfo addInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, ep);
		final Location loc = player.getLocation().clone();
		sendPacket(player, nearby, removeInfo);
		sendPacket(player, nearby, addInfo);
		player.teleport(yeet);
		new BukkitRunnable() {
			@Override
			public void run() {
				player.teleport(loc);
				ep.playerConnection.sendPacket(new PacketPlayOutRespawn(ep.dimension, ep.getWorld().getWorldData().getType(), ep.playerInteractManager.getGameMode()));
				player.updateInventory();
			}
		}.runTaskLater(plugin, 2L);
	}

	private static void sendPacket(Player who, Set<Player> whoElse, Packet packet) {
		sendPacket(who, packet);
		for (Player player : whoElse) {
			sendPacket(player, packet);
		}
	}

	private static void sendPacket(Player pl, Packet pk) {
		((CraftPlayer)pl).getHandle().playerConnection.sendPacket(pk);
	}

	private static Set<Player> getNearbyPlayersToUpdate(Player player) {
		Set<Player> res = new HashSet<>();
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			if (onlinePlayer.getUniqueId().equals(player.getUniqueId())) {
				continue;
			}
			if (onlinePlayer.getLocation().distance(player.getLocation()) < 200) {
				res.add(onlinePlayer);
			}
		}
		return res;
	}

}
