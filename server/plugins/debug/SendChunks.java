import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.minecraft.server.v1_14_R1.PacketPlayOutMapChunk;
import net.minecraft.server.v1_14_R1.PacketPlayOutUnloadChunk;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.CraftChunk;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class SendChunks implements Debugger, Runnable {

	private Player player;
	private RuntimeDebugger plugin;
	private int range = 16;
	private BukkitTask task;

	boolean unload = true;

	private int x;
	private int z;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			cs.sendMessage("Execute as player");
			return;
		}
		this.plugin = plugin;
		this.player = (Player) cs;
		this.task = Bukkit.getScheduler().runTaskTimer(plugin, this, 1L, 1L);
		this.x = -range;
		this.z = -range;
	}

	private synchronized void stopTask() {
		if (this.task != null) {
			this.task.cancel();
			this.task = null;
		}
	}

	@Override
	public void run() {
		if (unload) {
			while (true) {
				x++;
				if (x > range) {
					x = -range;
					z++;
				}
				if (z > range) {
					range--;
					if (range < 6) {
						range++;
						unload = false;
						this.player.sendMessage("Unloading done - resending");
						return;
					}
					x = -range;
					z = -range;
				}
				if (x == -range || x == range || z == -range || z == range) {
					Chunk playerChunk = this.player.getLocation().getChunk();
					Chunk chunk = this.player.getWorld().getChunkAt(playerChunk.getX() + x, playerChunk.getZ() + z);
					if (chunk == null) {
						this.player.sendMessage("Chunk not found at: " + playerChunk.getX() + x + "/" + playerChunk.getZ() + z);
					} else {
						this.player.sendMessage("Unsending chunk: " + chunk.getX() + "/" + chunk.getZ());
						unsendChunk(this.player, chunk);
					}
					return;
				}
			}
		} else {
			while (true) {
				x++;
				if (x > range) {
					x = -range;
					z++;
				}
				if (z > range) {
					range++;
					if (range > 16) {
						this.task.cancel();
						this.player.sendMessage("Script done!");
						return;
					}
					x = -range;
					z = -range;
				}
				if (x == -range || x == range || z == -range || z == range) {
					Chunk playerChunk = this.player.getLocation().getChunk();
					Chunk chunk = this.player.getWorld().getChunkAt(playerChunk.getX() + x, playerChunk.getZ() + z);
					if (chunk == null) {
						this.player.sendMessage("Chunk not found at: " + playerChunk.getX() + x + "/" + playerChunk.getZ() + z);
					} else {
						this.player.sendMessage("Sending chunk: " + chunk.getX() + "/" + chunk.getZ());
						sendChunk(this.player, chunk);
					}
					return;
				}
			}
		}
	}

	public void sendChunk(Player player, Chunk chunk) {
		PacketPlayOutMapChunk chunkPacket = new PacketPlayOutMapChunk(((CraftChunk)chunk).getHandle(), 65535);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(chunkPacket);
	}

	public void unsendChunk(Player player, Chunk chunk) {
		PacketPlayOutUnloadChunk unloadChunk = new PacketPlayOutUnloadChunk(chunk.getX(), chunk.getZ());
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(unloadChunk);
	}



}
