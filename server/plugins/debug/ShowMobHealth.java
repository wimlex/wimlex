import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.minecraft.server.v1_14_R1.DataWatcher;
import net.minecraft.server.v1_14_R1.EntityLiving;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityMetadata;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class ShowMobHealth implements Debugger, Debugger.Stoppable, Listener {

	private Player player;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (cs instanceof Player) {
			cs.sendMessage("Run this as player");
			return;
		}
		this.player = (Player) cs;
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (player != event.getPlayer()) {
			return;
		}
		for (Entity entity : player.getWorld().getEntities()) {
			if (!(entity instanceof LivingEntity)) {
				continue;
			}
			LivingEntity le = (LivingEntity) entity;
			if (le.getLocation().distance(this.player.getLocation()) > 32) {
				continue;
			}

			double max = le.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
			double val = le.getHealth();

			EntityLiving entityLivig = ((CraftLivingEntity)le).getHandle();
			
			String customNameBk = le.getCustomName();
			boolean customNameVisBk = le.isCustomNameVisible();
			
			le.setCustomName(String.format("%.1f/%.1f hp", val, max));
			le.setCustomNameVisible(true);
			DataWatcher dataWatcher = entityLivig.getDataWatcher();
			PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(
					le.getEntityId(),
					dataWatcher,
					true
			);
			((CraftPlayer)this.player).getHandle().playerConnection.sendPacket(metadata);
			le.setCustomName(customNameBk);
			le.setCustomNameVisible(customNameVisBk);
		}
	}

	@Override
	public void stop() {
	}

}
