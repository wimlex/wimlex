import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.local.LocalAddress;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import javax.crypto.SecretKey;
import java.net.SocketAddress;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class FakePlayerTest implements Debugger, Debugger.Stoppable, Listener {

	private static GameProfile GAME_PROFILE;
	private static FakePlayer FAKE_PLAYER;
	private static CraftPlayer FAKE_CRAFTPLAYER;
	private static RuntimeDebugger plugin;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		FakePlayerTest.plugin = plugin;
		if (!(cs instanceof Player)) {
			cs.sendMessage("Be a player");
			return;
		}
		Player p = (Player) cs;
		try {
			initUser();
			FakePlayer.server().getPlayerList().a(FAKE_PLAYER.getNetworkManager(), FAKE_PLAYER);
			FAKE_CRAFTPLAYER = FAKE_PLAYER.getBukkitEntity();
			Bukkit.getScheduler().runTaskLater(plugin, () -> {
				runScript(p);
				try {
					runHideScript();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}, 10L);
		} catch (Exception e) {
			e.printStackTrace();
			stop();
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (event.getPlayer().getUniqueId().equals(GAME_PROFILE.getId())) {
			event.setJoinMessage(null);
		}
	}

	private void runHideScript() throws Exception {
		// hide for online players
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			if (onlinePlayer != FAKE_CRAFTPLAYER) {
				onlinePlayer.hidePlayer(plugin, FAKE_CRAFTPLAYER);
			}
		}
		// hide from playerlist in multiplayer menu
		getPlayers().remove(FAKE_PLAYER);
		// hide from tab list
		PacketPlayOutPlayerInfo removePlayer = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, FAKE_PLAYER);
		getPlayerList().sendAll(removePlayer);
		// hide from mobs
		FAKE_CRAFTPLAYER.setInvulnerable(true);
	}

	private void runScript(Player p) {
		FAKE_CRAFTPLAYER.setHealth(20);
		FAKE_CRAFTPLAYER.teleport(p);
		FAKE_PLAYER.setPose(EntityPose.FALL_FLYING);
	}

	@Override
	public void stop() {
		FakePlayer.server().getPlayerList().disconnect(FAKE_PLAYER);
	}

	public PlayerList getPlayerList() {
		return FakePlayer.server().getPlayerList();
	}

	public List<EntityPlayer> getPlayers() {
		return FakePlayer.server().getPlayerList().players;
	}

	private void initUser() {
		GAME_PROFILE = new GameProfile(UUID.fromString("d6c1f06e-22a6-4d00-bc7d-b4a49a62a3bb"), "DeveloperBot");
		GAME_PROFILE.getProperties().put(
				"textures",
				new Property(
						"textures",
						"eyJ0aW1lc3RhbXAiOjE1NjMwODE3NzAxNDAsInByb2ZpbGVJZCI6ImQ2YzFmMDZlMjJhNjRkMDBiYzdkYjRhNDlhNjJhM2JiIiwicHJvZmlsZU5hbWUiOiJEZXZlbG9wZXJCb3QiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzU4MGRhNWNkMjJkMGRmZGQ2ODVlZWMwMTZmZTcxMjVjZWFlZThhZWJkYzVmYTY3MWI0ZjgwODgxNWI0NzgzMDUifX19",
						"ZMT6O27BzGlemCxFwBObc7EG6wt6zmbxzMGTCsgZkY4l9/nlz12UY3OvWUwWSC/kHTaUTmdcINJ9JbmfN2NqaQJwm381gnnkPgmrNW+o+A6H6PF2k+KUqc94m5zAIuBYq9VlzCJzLC31MJocEkJD+ZLp2/XUY03fPJKBsuEiA6BH6rxxsvAczlMIr8lBigLCK3UAi3rOec/UB6zxljyCiff8nBfJP4HfLxl3IiAYZRLk2V8SOVDJEZGWcF6Za0Fg3OdQFt6i4APoi63GqtgoOOFDqb5nDPagw3ThtUFEzJUhrTSYM2msg44cUhwjbBftMHcdHY8omUrCzPNSRsZY2XS3qWQGbLAaA7r63ByFDcGflMyI67mRLc1UQ+dsiCY8pKon1PSX+XKgZ5FNtnOY7KzR4Gb9H900fLFEJSNRJB4XoNn8TSnNpsJdZ07hEhP6ePaRxDDKOfmGHwup/2LI40R8YSZgBaN5aGcLBnevtpOGLtQM0F+OWHtBtyQGXzeWQfTn7lg52q7Rht+wyZBb1ploWWLcxrf0ATrobg6mw+Yq8azYS+eQhxULiQmtX79z0tpy1l0S606oNJpIhLHnz2KhdQBHNUaI5aSQI6gdYH41yvGOIWyv4lovfd6V/0c7Zzy5g9L6FDPe66WKUvQR1Z7qmzoKAABvlNoFY4X+VW4="
				)
		);
		FAKE_PLAYER = new FakePlayer();
	}

	static class FakePlayer extends EntityPlayer {

		private NetworkManager networkManager;

		public FakePlayer() {
			super(server(), worldServer(), gameProfile(), playerinteractmanager());
			this.networkManager = new FakeNetworkManager();
			new FakePlayerConnection(this);
		}

		public NetworkManager getNetworkManager() {
			return networkManager;
		}

		private static PlayerInteractManager playerinteractmanager() {
			return new FakeInteractManager(worldServer());
		}

		private static MinecraftServer server() {
			return ((CraftServer) Bukkit.getServer()).getHandle().getServer();
		}

		private static WorldServer worldServer() {
			return server().getWorldServer(DimensionManager.OVERWORLD);
		}

		private static GameProfile gameProfile() {
			return FakePlayerTest.GAME_PROFILE;
		}

		@Override
		public void setPose(EntityPose entitypose) {
			super.setPose(entitypose);
		}
	}

	static class FakeNetworkManager extends NetworkManager {

		public FakeNetworkManager() {
			super(EnumProtocolDirection.SERVERBOUND);
		}

		@Override
		public void channelActive(ChannelHandlerContext channelhandlercontext) throws Exception {
		}

		@Override
		public void setProtocol(EnumProtocol enumprotocol) {
		}

		@Override
		public void channelInactive(ChannelHandlerContext channelhandlercontext) throws Exception {
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext channelhandlercontext, Throwable throwable) {
		}

		@Override
		protected void channelRead0(ChannelHandlerContext channelhandlercontext, Packet<?> packet) throws Exception {
		}

		@Override
		public void setPacketListener(PacketListener packetlistener) {
		}

		@Override
		public void sendPacket(Packet<?> packet) {
		}

		@Override
		public void sendPacket(Packet<?> packet, GenericFutureListener<? extends Future<? super Void>> genericfuturelistener) {
		}

		@Override
		public void a() {
		}

		@Override
		public SocketAddress getSocketAddress() {
			return LocalAddress.ANY;
		}

		@Override
		public void close(IChatBaseComponent ichatbasecomponent) {
		}

		@Override
		public boolean isLocal() {
			return true;
		}

		@Override
		public void a(SecretKey secretkey) {
		}

		@Override
		public boolean isConnected() {
			return true;
		}

		@Override
		public boolean h() {
			return false;
		}

		@Override
		public PacketListener i() {
			return null;
		}

		@Override
		public IChatBaseComponent j() {
			return null;
		}

		@Override
		public void stopReading() {
		}

		@Override
		public void setCompressionLevel(int i) {
		}

		@Override
		public void handleDisconnection() {
		}

		@Override
		public SocketAddress getRawAddress() {
			return getSocketAddress();
		}

		@Override
		public boolean acceptInboundMessage(Object msg) throws Exception {
			return true;
		}

		@Override
		public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		}

		@Override
		public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		}

		@Override
		public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		}

		@Override
		public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		}

		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		}

		@Override
		public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
		}

		@Override
		protected void ensureNotSharable() {
		}

		@Override
		public boolean isSharable() {
			return true;
		}

		@Override
		public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		}

		@Override
		public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		}
	}

	static class FakePlayerConnection extends PlayerConnection {

		public FakePlayerConnection(FakePlayer player) {
			super(FakePlayer.server(), player.getNetworkManager(), player);
		}

		@Override
		public void tick() {
		}

		@Override
		public void syncPosition() {
		}

		@Override
		public NetworkManager a() {
			return super.a();
		}

		@Override
		public void disconnect(IChatBaseComponent ichatbasecomponent) {
		}

		@Override
		public void disconnect(String s) {
		}

		@Override
		public void a(PacketPlayInSteerVehicle packetplayinsteervehicle) {
		}

		@Override
		public void a(PacketPlayInVehicleMove packetplayinvehiclemove) {
		}

		@Override
		public void a(PacketPlayInTeleportAccept packetplayinteleportaccept) {
		}

		@Override
		public void a(PacketPlayInRecipeDisplayed packetplayinrecipedisplayed) {
		}

		@Override
		public void a(PacketPlayInAdvancements packetplayinadvancements) {
		}

		@Override
		public void a(PacketPlayInTabComplete packetplayintabcomplete) {
		}

		@Override
		public void a(PacketPlayInSetCommandBlock packetplayinsetcommandblock) {
		}

		@Override
		public void a(PacketPlayInSetCommandMinecart packetplayinsetcommandminecart) {
		}

		@Override
		public void a(PacketPlayInPickItem packetplayinpickitem) {
		}

		@Override
		public void a(PacketPlayInItemName packetplayinitemname) {
		}

		@Override
		public void a(PacketPlayInBeacon packetplayinbeacon) {
		}

		@Override
		public void a(PacketPlayInStruct packetplayinstruct) {
		}

		@Override
		public void a(PacketPlayInSetJigsaw packetplayinsetjigsaw) {
		}

		@Override
		public void a(PacketPlayInTrSel packetplayintrsel) {
		}

		@Override
		public void a(PacketPlayInBEdit packetplayinbedit) {
		}

		@Override
		public void a(PacketPlayInEntityNBTQuery packetplayinentitynbtquery) {
		}

		@Override
		public void a(PacketPlayInTileNBTQuery packetplayintilenbtquery) {
		}

		@Override
		public void a(PacketPlayInFlying packetplayinflying) {
		}

		@Override
		public void a(double d0, double d1, double d2, float f, float f1) {
		}

		@Override
		public void a(double d0, double d1, double d2, float f, float f1, PlayerTeleportEvent.TeleportCause cause) {
		}

		@Override
		public void a(double d0, double d1, double d2, float f, float f1, Set<PacketPlayOutPosition.EnumPlayerTeleportFlags> set) {
		}

		@Override
		public void a(double d0, double d1, double d2, float f, float f1, Set<PacketPlayOutPosition.EnumPlayerTeleportFlags> set, PlayerTeleportEvent.TeleportCause cause) {
		}

		@Override
		public void teleport(Location dest) {
		}

		@Override
		public void a(PacketPlayInBlockDig packetplayinblockdig) {
		}

		@Override
		public void a(PacketPlayInUseItem packetplayinuseitem) {
		}

		@Override
		public void a(PacketPlayInBlockPlace packetplayinblockplace) {
		}

		@Override
		public void a(PacketPlayInSpectate packetplayinspectate) {
		}

		@Override
		public void a(PacketPlayInResourcePackStatus packetplayinresourcepackstatus) {
		}

		@Override
		public void a(PacketPlayInBoatMove packetplayinboatmove) {
		}

		@Override
		public void a(IChatBaseComponent ichatbasecomponent) {
		}

		@Override
		public void sendPacket(Packet<?> packet) {
		}

		@Override
		public void a(Packet<?> packet, GenericFutureListener<? extends Future<? super Void>> genericfuturelistener) {
		}

		@Override
		public void a(PacketPlayInHeldItemSlot packetplayinhelditemslot) {
		}

		@Override
		public void a(PacketPlayInChat packetplayinchat) {
		}

		@Override
		public void chat(String s, boolean async) {
		}

		@Override
		public void a(PacketPlayInArmAnimation packetplayinarmanimation) {
		}

		@Override
		public void a(PacketPlayInEntityAction packetplayinentityaction) {
		}

		@Override
		public void a(PacketPlayInUseEntity packetplayinuseentity) {
		}

		@Override
		public void a(PacketPlayInClientCommand packetplayinclientcommand) {
		}

		@Override
		public void a(PacketPlayInCloseWindow packetplayinclosewindow) {
		}

		@Override
		public void a(PacketPlayInWindowClick packetplayinwindowclick) {
		}

		@Override
		public void a(PacketPlayInAutoRecipe packetplayinautorecipe) {
		}

		@Override
		public void a(PacketPlayInEnchantItem packetplayinenchantitem) {
		}

		@Override
		public void a(PacketPlayInSetCreativeSlot packetplayinsetcreativeslot) {
		}

		@Override
		public void a(PacketPlayInTransaction packetplayintransaction) {
		}

		@Override
		public void a(PacketPlayInUpdateSign packetplayinupdatesign) {
		}

		@Override
		public void a(PacketPlayInKeepAlive packetplayinkeepalive) {
		}

		@Override
		public void a(PacketPlayInAbilities packetplayinabilities) {
		}

		@Override
		public void a(PacketPlayInSettings packetplayinsettings) {
		}

		@Override
		public void a(PacketPlayInCustomPayload packetplayincustompayload) {
		}

		@Override
		public void a(PacketPlayInDifficultyChange packetplayindifficultychange) {
		}

		@Override
		public void a(PacketPlayInDifficultyLock packetplayindifficultylock) {
		}
	}

	static class FakeInteractManager extends PlayerInteractManager {
		public FakeInteractManager(WorldServer worldserver) {
			super(worldserver);
		}

		@Override
		public void setGameMode(EnumGamemode enumgamemode) {
			super.setGameMode(enumgamemode);
		}

		@Override
		public EnumGamemode getGameMode() {
			return super.getGameMode();
		}

		@Override
		public boolean c() {
			return super.c();
		}

		@Override
		public boolean isCreative() {
			return super.isCreative();
		}

		@Override
		public void b(EnumGamemode enumgamemode) {
			super.b(enumgamemode);
		}

		@Override
		public void a() {
			super.a();
		}

		@Override
		public void a(BlockPosition blockposition, EnumDirection enumdirection) {
			super.a(blockposition, enumdirection);
		}

		@Override
		public void a(BlockPosition blockposition) {
			super.a(blockposition);
		}

		@Override
		public void e() {
			super.e();
		}

		@Override
		public boolean breakBlock(BlockPosition blockposition) {
			return super.breakBlock(blockposition);
		}

		@Override
		public EnumInteractionResult a(EntityHuman entityhuman, World world, ItemStack itemstack, EnumHand enumhand) {
			return super.a(entityhuman, world, itemstack, enumhand);
		}

		@Override
		public EnumInteractionResult a(EntityHuman entityhuman, World world, ItemStack itemstack, EnumHand enumhand, MovingObjectPositionBlock movingobjectpositionblock) {
			return super.a(entityhuman, world, itemstack, enumhand, movingobjectpositionblock);
		}

		@Override
		public void a(WorldServer worldserver) {
			super.a(worldserver);
		}
	}

}
