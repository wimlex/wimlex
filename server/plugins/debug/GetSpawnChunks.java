import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

public class GetSpawnChunks implements Debugger {

	private CommandSender cs;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		this.cs = cs;
		World world = plugin.getServer().getWorlds().get(0);
		msg("Force loaded chunk count: " + world.getForceLoadedChunks().size());
		for (Chunk forceLoadedChunk : world.getForceLoadedChunks()) {
			msg("\tforce loaded chunk{x:%d,z:%d}", forceLoadedChunk.getX(), forceLoadedChunk.getZ());
		}
		msg("Loaded chunk count: " + world.getLoadedChunks().length);
		for (Chunk loadedChunk : world.getLoadedChunks()) {
			msg("\tloaded chunk{x:%d,z:%d}", loadedChunk.getX(), loadedChunk.getZ());
		}
	}

	void msg(String msg, Object... args) {
		if (args != null && args.length > 0) {
			msg = String.format(msg, args);
		}
		this.cs.sendMessage(ChatColor.YELLOW + msg);
	}

}
