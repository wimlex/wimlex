#!/bin/bash

if [ "$#" -lt 1 ]; then
	echo "Usage: $0 <download|install>"
	exit 1
fi

source ~/config/setup.sh


case "$1" in
download)
	/srv/wimlex-jars/check-jar.sh spigot-$SPIGOT_VERSION 86400
	if [ $? -eq 0 ]; then
		cp /srv/wimlex-jars/spigot-$SPIGOT_VERSION.jar ~/buildtools
		exit 1;
	fi
	cd ~/buildtools
	rm -rf BuildTools.jar
	./download-latest-buildtools.sh
	./download-spigot.sh
	exit 1
	;;
install)
	cp ~/buildtools/spigot-$SPIGOT_VERSION.jar ~/server
	exit 1
	;;
*)
	echo "Invalid option: $1"
	exit 1
esac
