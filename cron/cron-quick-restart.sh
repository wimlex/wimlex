#!/bin/bash

source ~/config/setup.sh

RUNNING=false

if screen -list | grep -i "\.$SPIGOT_SCREENNAME"; then
	RUNNING=true
fi

if [ "$RUNNING" = "true" ]; then
	echo "say Quick restart in 30 seconds"
	~/server/send-say.sh "Quick restart in 30 seconds"
        sleep 25s
	echo "say Quick restart in 5 seconds"
        ~/server/send-say.sh "Quick restart in 5 seconds"
        sleep 4s
	echo "say Quick restarting"
        ~/server/send-say.sh "Quick restarting"
        sleep 1s
	echo "save-all"
        ~/server/send-cmd.sh "save-all"
        sleep 10s
	echo "restarting"
        ~/server/send-cmd.sh "stop"
fi

