#!/bin/bash

source ~/config/setup.sh

# test if version exists
curl https://hub.spigotmc.org/versions/ | grep -i $SPIGOT_VERSION

if [ $? -eq 0 ]; then
	java -jar BuildTools.jar --rev=$SPIGOT_VERSION
	exit 0;
fi

echo "Unable to download version '$SPIGOT_VERSION'. Version does not exist."
